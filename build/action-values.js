"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.list = exports.names = void 0;
// @author laudeon
// @license MIT
const names = ["Partager des bonnes pratiques​", "Être en appui sur une autre session LAB", "Mieux se connaître​", 'Activer son réseau pour les autres​', 'Partager les résultats d\'une session LAB', 'Inviter des facilitateurs en aide/​observation de sessions', 'Co-animer la communauté', 'Multiplex', 'Utilisation de Pôle', 'Répondre à des questions​', 'Partager des ressources documentaires', 'Echanges, prêts de matériel​​', 'Participer à un groupe de travail de la communauté', 'Aider à créer un réseau local d\'intervenants/experts​​​'];
exports.names = names;
const list = [{
  name: "Partager des bonnes pratiques​",
  challengeScore: 380,
  individualScore: 95,
  taggedIndividualScore: 30
}, {
  name: "Être en appui sur une autre session LAB",
  challengeScore: 370,
  individualScore: 90,
  taggedIndividualScore: 30
}, {
  name: "Mieux se connaître​",
  challengeScore: 310,
  individualScore: 80,
  taggedIndividualScore: 25
}, {
  name: 'Activer son réseau pour les autres​',
  challengeScore: 285,
  individualScore: 75,
  taggedIndividualScore: 25
}, {
  name: 'Partager les résultats d\'une session LAB',
  challengeScore: 280,
  individualScore: 70,
  taggedIndividualScore: 20
}, {
  name: 'Inviter des facilitateurs en aide/​observation de sessions',
  challengeScore: 260,
  individualScore: 65,
  taggedIndividualScore: 20
}, {
  name: 'Co-animer la communauté',
  challengeScore: 260,
  individualScore: 65,
  taggedIndividualScore: 20
}, {
  name: 'Multiplex',
  challengeScore: 240,
  individualScore: 50,
  taggedIndividualScore: 20
}, {
  name: 'Utilisation de Pôle',
  challengeScore: 200,
  individualScore: 50,
  taggedIndividualScore: 15
}, {
  name: 'Répondre à des questions​',
  challengeScore: 195,
  individualScore: 50,
  taggedIndividualScore: 15
}, {
  name: 'Partager des ressources documentaires',
  challengeScore: 190,
  individualScore: 45,
  taggedIndividualScore: 10
}, {
  name: 'Echanges, prêts de matériel​​',
  challengeScore: 50,
  individualScore: 15,
  taggedIndividualScore: 5
}, {
  name: 'Participer à un groupe de travail de la communauté',
  challengeScore: 60,
  individualScore: 10,
  taggedIndividualScore: 5
}, {
  name: 'Aider à créer un réseau local d\'intervenants/experts​​​',
  challengeScore: 50,
  individualScore: 10,
  taggedIndividualScore: 5
}];
exports.list = list;