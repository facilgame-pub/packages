# exported
- actions
    - names: actions' name in an array. `["<name>", "..."]`
    - list: action objects list in an array.`[{...}, {...}]`

- labs
    - list: lab object array. `[{label: ..., value: ...}, {...}]`
    - getLabelFromValue()
