export const list = [
  { label: "Auvergne-Rhône-Alpes", value: "auvergne-rhone-alpes" },
  { label: "Bourgogne-Franche-Comté", value: "bourgogne-franche-comté" },
  { label: "Bretagne", value: "bretagne" },
  { label: "Corse", value: "corse" },
  { label: "Centre-Val de Loire", value: "centre-val-de-loire" },
  { label: "DSI La Paz", value: "dsi-la-paz" },
  { label: "Guadeloupe", value: "guadeloupe" },
  { label: "Guyane", value: "guyane" },
  { label: "Hauts de France", value: "hauts-de-france" },
  { label: "Grand Est", value: "grand-est" },
  { label: "Ile-de-France", value: "ile-de-france" },
  { label: "Ile de la Réunion", value: "ile-de-la-réunion" },
  { label: "LAB National", value: "lab-national" },
  { label: "Lorient Ville", value: "lorient-ville" },
  { label: "Martinique", value: "martinique" },
  { label: "Normandie", value: "normandie" },
  { label: "Mayotte", value: "mayotte" },
  { label: "Nouvelle-Aquitaine", value: "nouvelle-aquitaine" },
  { label: "Occitanie", value: "occitanie" },
  { label: "Pays de la Loire", value: "pays-de-la-loire" },
  { label: "Provence-Alpes-Côte d’Azur", value: "provence-alpes-cote-d-azur" },
]

export function getLabelFromValue (value) {
  const lab = labValues.find(lab => lab.value === value)
  if (!lab) return 'aucun lab'
  return lab.label
}